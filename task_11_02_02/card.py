# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_11_02_02.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


class Card:
    """Класс Card представляет игральную карту.

    Поля экземпляра класса:
      - self._value (int): значение карты;
      - self.is_face (bool): True, если карта лежит лицом вверх.

    Методы экземпляра класса:
      - self.turn_face(): переворачивает карту лицом вверх.
      - self.turn_back(): переворачивает карту рубашкой вверх.

    Атрибуты класса:
      - BACK (str): рубашка карты.

    Свойства:
      - value (int): значение карты (только для чтения).
    """

    BACK = "X"

    def __init__(self, value):
        """Инициализация класса-карты.

        По умолчанию карта должна быть рубашкой вверх (лицом вниз).

        Параметры:
          - value (int): значение карты.

        Необходимо удостовериться, что 'value' - int (assert).
        """
        raise NotImplementedError
        # Уберите raise и допишите код

    def __str__(self):
        """Вернуть строковое представление карты.

        Если карта рубашкой вниз, вернуть значение, иначе 'BACK'.
        """
        raise NotImplementedError
        # Уберите raise и допишите код

    def turn_face(self):
        """Перевернуть карту лицом вверх."""
        raise NotImplementedError
        # Уберите raise и допишите код

    def turn_back(self):
        """Перевернуть карту рубашкой вверх."""
        raise NotImplementedError
        # Уберите raise и допишите код

    @property
    def value(self):
        """Вернуть значение карты."""
        raise NotImplementedError
        # Уберите raise и допишите код
