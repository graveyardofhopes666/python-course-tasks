# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_13_02_03.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


import sys
import os
import datetime

# Лог-файл
# Имя лог-файла задано в коде - "log.txt" в папке приложения
app_path = os.path.dirname(os.path.realpath(sys.argv[0]))
log_filename = os.path.join(app_path, "log.txt")

agency_name = !!!


def log(message):
    """Вывести на экран 'message' с указанием текущего времени
    формате 0001-01-01 00:00:00 (год, месяц, число, часы, минуты, секунды):
      - на экран;
      - в лог-файл (дописать в конец файла).
    """
    raise NotImplementedError
    # Уберите raise и дополните код
